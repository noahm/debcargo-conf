This patch is based on the upstream commit described below, adapted for use
in the Debian package by Peter Michael Green.

commit fd9d4e24f0ba8a711a98ab9dd868e4fa1ff48320
Author: Markus Stange <markus.stange@gmail.com>
Date:   Wed Jan 17 16:14:52 2024 -0500

    Update zerocopy and use Unaligned.

Index: macho-unwind-info/src/raw/format.rs
===================================================================
--- macho-unwind-info.orig/src/raw/format.rs
+++ macho-unwind-info/src/raw/format.rs
@@ -1,12 +1,12 @@
 use std::fmt::Debug;
-use zerocopy::FromBytes;
+use zerocopy_derive::{FromBytes, FromZeroes, Unaligned};
 
 use super::unaligned::{U16, U32};
 
 // Written with help from https://gankra.github.io/blah/compact-unwinding/
 
 /// The `__unwind_info` header.
-#[derive(FromBytes, Debug, Clone, Copy)]
+#[derive(Unaligned, FromZeroes, FromBytes, Debug, Clone, Copy)]
 #[repr(C)]
 pub struct CompactUnwindInfoHeader {
     /// The version. Only version 1 is currently defined
@@ -40,7 +40,7 @@ pub struct CompactUnwindInfoHeader {
 }
 
 /// One element of the array of pages.
-#[derive(FromBytes, Clone, Copy)]
+#[derive(Unaligned, FromZeroes, FromBytes, Clone, Copy)]
 #[repr(C)]
 pub struct PageEntry {
     /// The first address mapped by this page.
@@ -63,7 +63,7 @@ pub struct PageEntry {
 }
 
 /// A non-compressed page.
-#[derive(FromBytes, Debug, Clone, Copy)]
+#[derive(Unaligned, FromZeroes, FromBytes, Debug, Clone, Copy)]
 #[repr(C)]
 pub struct RegularPage {
     /// Always 2 (use to distinguish from CompressedPage).
@@ -75,7 +75,7 @@ pub struct RegularPage {
 }
 
 /// A "compressed" page.
-#[derive(FromBytes, Debug, Clone, Copy)]
+#[derive(Unaligned, FromZeroes, FromBytes, Debug, Clone, Copy)]
 #[repr(C)]
 pub struct CompressedPage {
     /// Always 3 (use to distinguish from RegularPage).
@@ -98,12 +98,12 @@ pub struct CompressedPage {
 }
 
 /// An opcode.
-#[derive(FromBytes, Debug, Clone, Copy)]
+#[derive(Unaligned, FromZeroes, FromBytes, Debug, Clone, Copy)]
 #[repr(C)]
 pub struct Opcode(pub U32);
 
 /// A function entry from a non-compressed page.
-#[derive(FromBytes, Debug, Clone, Copy)]
+#[derive(Unaligned, FromZeroes, FromBytes, Debug, Clone, Copy)]
 #[repr(C)]
 pub struct RegularFunctionEntry {
     /// The address in the binary for this function entry (absolute).
Index: macho-unwind-info/src/raw/unaligned.rs
===================================================================
--- macho-unwind-info.orig/src/raw/unaligned.rs
+++ macho-unwind-info/src/raw/unaligned.rs
@@ -1,9 +1,11 @@
 use std::fmt::Debug;
 
-use zerocopy::{FromBytes, Unaligned};
+use zerocopy_derive::{FromBytes, FromZeroes, Unaligned};
 
 /// An unaligned little-endian `u32` value.
-#[derive(Unaligned, FromBytes, Default, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
+#[derive(
+    Unaligned, FromZeroes, FromBytes, Default, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash,
+)]
 #[repr(transparent)]
 pub struct U32([u8; 4]);
 
@@ -26,7 +28,9 @@ impl Debug for U32 {
 }
 
 /// An unaligned little-endian `u16` value.
-#[derive(Unaligned, FromBytes, Default, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
+#[derive(
+    Unaligned, FromZeroes, FromBytes, Default, Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash,
+)]
 #[repr(transparent)]
 pub struct U16([u8; 2]);
 
Index: macho-unwind-info/src/reader.rs
===================================================================
--- macho-unwind-info.orig/src/reader.rs
+++ macho-unwind-info/src/reader.rs
@@ -1,22 +1,22 @@
-use zerocopy::{FromBytes, LayoutVerified};
+use zerocopy::{FromBytes, Ref, Unaligned};
 
 pub trait Reader {
-    fn read_at<T: FromBytes>(&self, offset: u64) -> Option<&T>;
-    fn read_slice_at<T: FromBytes>(&self, offset: u64, len: usize) -> Option<&[T]>;
+    fn read_at<T: FromBytes + Unaligned>(&self, offset: u64) -> Option<&T>;
+    fn read_slice_at<T: FromBytes + Unaligned>(&self, offset: u64, len: usize) -> Option<&[T]>;
 }
 
-impl<'a> Reader for [u8] {
-    fn read_at<T: FromBytes>(&self, offset: u64) -> Option<&T> {
+impl Reader for [u8] {
+    fn read_at<T: FromBytes + Unaligned>(&self, offset: u64) -> Option<&T> {
         let offset: usize = offset.try_into().ok()?;
         let end: usize = offset.checked_add(core::mem::size_of::<T>())?;
-        let lv = LayoutVerified::<&[u8], T>::new(self.get(offset..end)?)?;
+        let lv = Ref::<&[u8], T>::new_unaligned(self.get(offset..end)?)?;
         Some(lv.into_ref())
     }
 
-    fn read_slice_at<T: FromBytes>(&self, offset: u64, len: usize) -> Option<&[T]> {
+    fn read_slice_at<T: FromBytes + Unaligned>(&self, offset: u64, len: usize) -> Option<&[T]> {
         let offset: usize = offset.try_into().ok()?;
         let end: usize = offset.checked_add(core::mem::size_of::<T>().checked_mul(len)?)?;
-        let lv = LayoutVerified::<&[u8], [T]>::new_slice(self.get(offset..end)?)?;
+        let lv = Ref::<&[u8], [T]>::new_slice_unaligned(self.get(offset..end)?)?;
         Some(lv.into_slice())
     }
 }
Index: macho-unwind-info/Cargo.toml
===================================================================
--- macho-unwind-info.orig/Cargo.toml
+++ macho-unwind-info/Cargo.toml
@@ -32,6 +32,10 @@ name = "unwindinfolookup"
 version = "1.0.30"
 
 [dependencies.zerocopy]
-version = "0.6.1"
+version = "0.7.32"
+
+[dependencies.zerocopy-derive]
+version = "0.7.32"
+
 [dev-dependencies.object]
 version = "0.28.2"

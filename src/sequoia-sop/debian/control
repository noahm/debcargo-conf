Source: rust-sequoia-sop
Section: utils
Priority: optional
Build-Depends: debhelper-compat (= 13),
 dh-sequence-cargo,
 cargo:native,
 rustc:native,
 libstd-rust-dev,
 librust-anyhow-1+default-dev (>= 1.0.18-~~),
 librust-sequoia-openpgp-1+default-dev (>= 1.20-~~),
 librust-sequoia-openpgp-1-dev (>= 1.20-~~),
 librust-sequoia-wot-0.13-dev,
 librust-sop-0.8+default-dev,
 bash-completion
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Alexander Kjäll <alexander.kjall@gmail.com>,
 Daniel Kahn Gillmor <dkg@fifthhorseman.net>,
 Holger Levsen <holger@debian.org>
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/sequoia-sop]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/sequoia-sop
Homepage: https://sequoia-pgp.org/
X-Cargo-Crate: sequoia-sop
Rules-Requires-Root: no

Package: librust-sequoia-sop-dev
Section: rust
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-anyhow-1+default-dev (>= 1.0.18-~~),
 librust-sequoia-openpgp-1+default-dev (>= 1.20-~~),
 librust-sequoia-openpgp-1-dev (>= 1.20-~~),
 librust-sequoia-wot-0.13-dev,
 librust-sop-0.8+cli-dev,
 librust-sop-0.8+cliv-dev,
 librust-sop-0.8+default-dev
Provides:
 librust-sequoia-sop+cli-dev (= ${binary:Version}),
 librust-sequoia-sop+cliv-dev (= ${binary:Version}),
 librust-sequoia-sop+default-dev (= ${binary:Version}),
 librust-sequoia-sop-0-dev (= ${binary:Version}),
 librust-sequoia-sop-0+cli-dev (= ${binary:Version}),
 librust-sequoia-sop-0+cliv-dev (= ${binary:Version}),
 librust-sequoia-sop-0+default-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36+cli-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36+cliv-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36+default-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36.0-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36.0+cli-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36.0+cliv-dev (= ${binary:Version}),
 librust-sequoia-sop-0.36.0+default-dev (= ${binary:Version})
Description: Stateless OpenPGP Command Line Interface using Sequoia - Rust source code
 sqop offers a Rust-based implementation of the Stateless OpenPGP
 Command Line Interface.
 .
 This standards-derived interface is an implementation of
 https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/,
 intended to be used in interoperability tests and simple OpenPGP
 commands, while leaving no trace in the filesystem.
 Source code for Debianized Rust crate "sequoia-sop"

Package: sqop
Architecture: any
Multi-Arch: allowed
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 sopv (= 1.0),
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Stateless OpenPGP Command Line Interface using Sequoia
 sqop offers a Rust-based implementation of the Stateless OpenPGP
 Command Line Interface.
 .
 This standards-derived interface is an implementation of
 https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/,
 intended to be used in interoperability tests and simple OpenPGP
 commands, while leaving no trace in the filesystem.
 This package contains the following binaries built from the Rust crate
 "sequoia-sop":
  - sqop

Package: sqopv
Architecture: any
Multi-Arch: allowed
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 sopv (= 1.0),
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
Static-Built-Using: ${cargo:Static-Built-Using}
Description: Stateless OpenPGP Verification-only Command Line Interface using Sequoia
 sqopv offers a Rust-based implementation of the Stateless OpenPGP
 Command Line Interface's verification-only subset (also known as "sopv").
 .
 This standards-derived interface is an implementation of
 https://datatracker.ietf.org/doc/draft-dkg-openpgp-stateless-cli/,
 intended to be used in interoperability tests and simple OpenPGP
 commands, while leaving no trace in the filesystem.
 This package contains the following binaries built from the Rust crate
 "sequoia-sop":
  - sqopv

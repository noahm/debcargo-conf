This patch is based on the upstream commit described below, adapted for use
in the Debian package.

From da6bdca5775f79fd64934fbe89238a34d3458142 Mon Sep 17 00:00:00 2001
From: Dirkjan Ochtman <dirkjan@ochtman.nl>
Date: Fri, 4 Nov 2022 23:11:47 +0100
Subject: [PATCH] proto: upgrade to Quinn 0.9

---
 src/quic/quic_client_stream.rs |  23 ++--
 src/quic/quic_server.rs        |  29 +++--
 4 files changed, 108 insertions(+), 76 deletions(-)

Index: trust-dns-proto/src/quic/quic_client_stream.rs
===================================================================
--- trust-dns-proto.orig/src/quic/quic_client_stream.rs
+++ trust-dns-proto/src/quic/quic_client_stream.rs
@@ -15,7 +15,7 @@ use std::{
 };
 
 use futures_util::{future::FutureExt, stream::Stream};
-use quinn::{ClientConfig, Connection, Endpoint, NewConnection, OpenBi, TransportConfig, VarInt};
+use quinn::{ClientConfig, Connection, Endpoint, TransportConfig, VarInt};
 use rustls::{version::TLS13, ClientConfig as TlsClientConfig};
 
 use crate::{
@@ -52,8 +52,11 @@ impl QuicClientStream {
         QuicClientStreamBuilder::default()
     }
 
-    async fn inner_send(stream: OpenBi, message: DnsRequest) -> Result<DnsResponse, ProtoError> {
-        let (send_stream, recv_stream) = stream.await?;
+    async fn inner_send(
+        connection: Connection,
+        message: DnsRequest,
+    ) -> Result<DnsResponse, ProtoError> {
+        let (send_stream, recv_stream) = connection.open_bi().await?;
 
         // RFC: The mapping specified here requires that the client selects a separate
         //  QUIC stream for each query. The server then uses the same stream to provide all the response messages for that query.
@@ -115,9 +118,7 @@ impl DnsRequestSender for QuicClientStre
             panic!("can not send messages after stream is shutdown")
         }
 
-        let connection = self.quic_connection.open_bi();
-
-        Box::pin(Self::inner_send(connection, message)).into()
+        Box::pin(Self::inner_send(self.quic_connection.clone(), message)).into()
     }
 
     fn shutdown(&mut self) {
@@ -189,7 +190,7 @@ impl QuicClientStreamBuilder {
         let socket = socket.into_std()?;
 
         let endpoint_config = quic_config::endpoint();
-        let (mut endpoint, _incoming) = Endpoint::new(endpoint_config, None, socket)?;
+        let mut endpoint = Endpoint::new(endpoint_config, None, socket, quinn::TokioRuntime)?;
 
         // ensure the ALPN protocol is set correctly
         let mut crypto_config = self.crypto_config;
@@ -199,14 +200,14 @@ impl QuicClientStreamBuilder {
         let early_data_enabled = crypto_config.enable_early_data;
 
         let mut client_config = ClientConfig::new(Arc::new(crypto_config));
-        client_config.transport = self.transport_config;
+        client_config.transport_config(self.transport_config.clone());
 
         endpoint.set_default_client_config(client_config);
 
         let connecting = endpoint.connect(name_server, &dns_name)?;
         // TODO: for Client/Dynamic update, don't use RTT, for queries, do use it.
 
-        let connection = if early_data_enabled {
+        let quic_connection = if early_data_enabled {
             match connecting.into_0rtt() {
                 Ok((new_connection, _)) => new_connection,
                 Err(connecting) => connecting.await?,
@@ -214,10 +215,6 @@ impl QuicClientStreamBuilder {
         } else {
             connecting.await?
         };
-        let NewConnection {
-            connection: quic_connection,
-            ..
-        } = connection;
 
         Ok(QuicClientStream {
             quic_connection,
Index: trust-dns-proto/src/quic/quic_server.rs
===================================================================
--- trust-dns-proto.orig/src/quic/quic_server.rs
+++ trust-dns-proto/src/quic/quic_server.rs
@@ -7,8 +7,7 @@
 
 use std::{io, net::SocketAddr, sync::Arc};
 
-use futures_util::StreamExt;
-use quinn::{Endpoint, Incoming, IncomingBiStreams, ServerConfig};
+use quinn::{Connection, Endpoint, ServerConfig};
 use rustls::{server::ServerConfig as TlsServerConfig, version::TLS13, Certificate, PrivateKey};
 
 use crate::{error::ProtoError, udp::UdpSocket};
@@ -21,7 +20,6 @@ use super::{
 /// A DNS-over-QUIC Server, see QuicClientStream for the client counterpart
 pub struct QuicServer {
     endpoint: Endpoint,
-    incoming: Incoming,
 }
 
 impl QuicServer {
@@ -58,9 +56,14 @@ impl QuicServer {
         let socket = socket.into_std()?;
 
         let endpoint_config = quic_config::endpoint();
-        let (endpoint, incoming) = Endpoint::new(endpoint_config, Some(server_config), socket)?;
+        let endpoint = Endpoint::new(
+            endpoint_config,
+            Some(server_config),
+            socket,
+            quinn::TokioRuntime,
+        )?;
 
-        Ok(Self { endpoint, incoming })
+        Ok(Self { endpoint })
     }
 
     /// Get the next incoming stream
@@ -69,18 +72,14 @@ impl QuicServer {
     ///
     /// A remote connection that could have many potential bi-directional streams and the remote socket address
     pub async fn next(&mut self) -> Result<Option<(QuicStreams, SocketAddr)>, ProtoError> {
-        let connecting = match self.incoming.next().await {
+        let connecting = match self.endpoint.accept().await {
             Some(conn) => conn,
             None => return Ok(None),
         };
 
         let remote_addr = connecting.remote_address();
-        let conn = connecting.await?;
-        let streams = QuicStreams {
-            incoming_bi_streams: conn.bi_streams,
-        };
-
-        Ok(Some((streams, remote_addr)))
+        let connection = connecting.await?;
+        Ok(Some((QuicStreams { connection }, remote_addr)))
     }
 
     /// Returns the address this server is listening on
@@ -94,13 +93,13 @@ impl QuicServer {
 
 /// A stream of bi-directional QUIC streams
 pub struct QuicStreams {
-    incoming_bi_streams: IncomingBiStreams,
+    connection: Connection,
 }
 
 impl QuicStreams {
     /// Get the next bi directional stream from the client
     pub async fn next(&mut self) -> Option<Result<QuicStream, ProtoError>> {
-        match self.incoming_bi_streams.next().await? {
+        match self.connection.accept_bi().await {
             Ok((send_stream, receive_stream)) => {
                 Some(Ok(QuicStream::new(send_stream, receive_stream)))
             }
--- trust-dns-proto.orig/Cargo.toml
+++ trust-dns-proto/Cargo.toml
@@ -117,7 +117,7 @@ features = [
 optional = true
 
 [dependencies.quinn]
-version = "0.8.2"
+version = "0.9"
 optional = true
 
 [dependencies.rand]

rust-sequoia-sqv (1.2.1-5) unstable; urgency=medium

  * Package sequoia-sqv 1.2.1 from crates.io using debcargo 2.7.5
  * d/copyright:
    - drop juga from upstream contacts.
    - use consistent naming to refer to GPL2+, thanks lintian.
  * Ship upstream NEWS file, thanks Guillem Jover for the idea.
    Closes: #1089978.

 -- Holger Levsen <holger@debian.org>  Wed, 25 Dec 2024 19:59:56 +0100

rust-sequoia-sqv (1.2.1-4) unstable; urgency=medium

  * Package sequoia-sqv 1.2.1 from crates.io using debcargo 2.7.5
  * d/rules:
    - on 64bit archs: build with lto="fat", codegen-units=1 and opt-level="z",
      for roughly 50% smaller binary.
    - on 32bit archs: disable lto optimisation.

 -- Holger Levsen <holger@debian.org>  Fri, 20 Dec 2024 11:30:10 +0100

rust-sequoia-sqv (1.2.1-3) unstable; urgency=medium

  * Package sequoia-sqv 1.2.1 from crates.io using debcargo 2.7.1
    - debian/patches: add dep3 headers.

 -- Holger Levsen <holger@debian.org>  Sat, 19 Oct 2024 15:53:21 +0200

rust-sequoia-sqv (1.2.1-2) unstable; urgency=medium

  * Package sequoia-sqv 1.2.1 from crates.io using debcargo 2.7.0
  * Ship fish and zsh shell completions again, now generated from upstream.
  * Ship upstream sqv manpage instead of custom one.
  * Add relax-deps.patch to allow build with sequoia-policy-config 0.7.

 -- Holger Levsen <holger@debian.org>  Fri, 11 Oct 2024 21:34:46 +0200

rust-sequoia-sqv (1.2.1-1) unstable; urgency=medium

  * Package sequoia-sqv 1.2.1 from crates.io using debcargo 2.7.0
  * Add Alexander Kjäll and myself as uploaders.
  * Drop sqv.install and thus fish and zsh shell completions temporarily to
    get his new version into unstable.

 -- Holger Levsen <holger@debian.org>  Mon, 07 Oct 2024 17:31:05 +0200

rust-sequoia-sqv (1.1.0-1) unstable; urgency=medium

  * Package sequoia-sqv 1.1.0 from crates.io using debcargo 2.5.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 02 Feb 2022 03:43:10 -0500

rust-sequoia-sqv (1.0.0-2) unstable; urgency=medium

  * Package sequoia-sqv 1.0.0 from crates.io using debcargo 2.4.3
  * include tab completion for bash, zsh, and fish

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 28 Jan 2021 17:57:31 -0500

rust-sequoia-sqv (1.0.0-1) unstable; urgency=medium

  * Package sequoia-sqv 1.0.0 from crates.io using debcargo 2.4.3

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 16 Dec 2020 12:43:47 -0500

rust-sequoia-sqv (0.21.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.21.0 from crates.io using debcargo 2.4.3

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 15 Dec 2020 00:50:32 -0500

rust-sequoia-sqv (0.20.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.20.0 from crates.io using debcargo 2.4.3

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 15 Oct 2020 00:16:13 -0400

rust-sequoia-sqv (0.18.0-2) unstable; urgency=medium

  * Package sequoia-sqv 0.18.0 from crates.io using debcargo 2.4.2
  * update sqv.1

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 06 Aug 2020 00:56:54 -0400

rust-sequoia-sqv (0.18.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.18.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 05 Aug 2020 19:35:28 -0400

rust-sequoia-sqv (0.17.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.17.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 19 Jun 2020 15:59:09 -0400

rust-sequoia-sqv (0.16.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.16.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 24 Apr 2020 19:09:03 -0400

rust-sequoia-sqv (0.15.0-2) unstable; urgency=medium

  * Package sequoia-sqv 0.15.0 from crates.io using debcargo 2.4.2
  * update sqv.1 manpage

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 06 Mar 2020 16:32:16 -0500

rust-sequoia-sqv (0.15.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.15.0 from crates.io using debcargo 2.4.2
  * drop patch already applied upstream.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 06 Mar 2020 14:43:36 -0500

rust-sequoia-sqv (0.14.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.14.0 from crates.io using debcargo 2.4.2
  * Relax versoined dependency on colored

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 19 Feb 2020 17:32:06 -0500

rust-sequoia-sqv (0.13.0-2) unstable; urgency=medium

  * Update sqv.1 manually
  * Package sequoia-sqv 0.13.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 15 Jan 2020 17:24:58 -0500

rust-sequoia-sqv (0.13.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.13.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 15 Jan 2020 12:47:14 -0500

rust-sequoia-sqv (0.12.0-2) unstable; urgency=medium

  * rebuild for source-only upload now that it has passed NEW

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 13 Dec 2019 09:32:49 -0500

rust-sequoia-sqv (0.12.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.12.0 from crates.io using debcargo 2.4.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 04 Dec 2019 13:33:08 -0500

rust-sequoia-sqv (0.11.0-1) unstable; urgency=medium

  * Package sequoia-sqv 0.11.0 from crates.io using debcargo 2.4.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 18 Nov 2019 07:32:52 +0800
